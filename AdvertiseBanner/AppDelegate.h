//
//  AppDelegate.h
//  AdvertiseBanner
//
//  Created by Carlos Thurber B. on 06/09/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
