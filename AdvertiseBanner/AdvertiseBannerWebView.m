//
//  AdvertiseBannerWebView.m
//  AdvertiseBanner
//
//  Created by Carlos Thurber B. on 06/09/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import "AdvertiseBannerWebView.h"

#define RELOAD_TIME 60*60
#define RECOVER_TIME 10

@interface AdvertiseBannerWebView ()
{
    BOOL _bannerLoaded;
    AdvertiseBannerType _adType;
}

@end

@implementation AdvertiseBannerWebView

- (id) initWithAdType:(AdvertiseBannerType)adType
{
    if (adType == AdvertiseBannerType_iPhone) {
        self = [self initWithFrame:CGRectMake(0, 0, 320, 50)];
    }
    else if (adType == AdvertiseBannerType_iPad) {
        self = [self initWithFrame:CGRectMake(0, 0, 768, 66)];
    }
    _adType = adType;
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scrollView.scrollEnabled = NO;
        self.delegate = self;
    }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    NSLog(@"AdvertiseBannerWebView: ASKING FOR SIZE THAT FITS %f %f",size.width,size.height);
    if (_adType == AdvertiseBannerType_iPhone) {
        return CGSizeMake(size.width, 50);
    }
    else {
        return CGSizeMake(size.width, 66);
    }
}

- (void) setUrlString:(NSString *)urlString
{
    _urlString = [NSString stringWithFormat:@"%@%@&scale=%.0f",urlString,_adType==AdvertiseBannerType_iPhone?@"?type=iPhone":@"?type=iPad",[[UIScreen mainScreen ]scale]];
    [self loadAdvertise];
}

- (void) loadAdvertise
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.urlString]];
    [self loadRequest:request];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"AdvertiseBannerWebView: DEBE INICIAR CARGA? %@",request.URL.absoluteString);
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [self.adBannerDelegate openURL:request.URL];
        return NO;
    }
    else {
        return YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    _bannerLoaded = NO;
    if ([self.adBannerDelegate respondsToSelector:@selector(adBannerViewWillLoadAd:)]) {
        [self.adBannerDelegate adBannerViewWillLoadAd:self];
    }
    NSLog(@"AdvertiseBannerWebView: INICIO CARGA");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    _bannerLoaded = YES;
    [self.adBannerDelegate adBannerViewDidLoadAd:self];
    [NSTimer scheduledTimerWithTimeInterval:RELOAD_TIME target:self selector:@selector(newAttempTimer:) userInfo:nil repeats:NO];
    NSLog(@"AdvertiseBannerWebView: TERMINO CARGA");
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    _bannerLoaded = NO;
    [self.adBannerDelegate adBannerView:self didFailToReceiveAdWithError:error];
    [NSTimer scheduledTimerWithTimeInterval:RECOVER_TIME target:self selector:@selector(newAttempTimer:) userInfo:nil repeats:NO];
    NSLog(@"AdvertiseBannerWebView: FALLO CARGA");
}

#pragma mark - 

- (void) newAttempTimer:(NSTimer*)timer
{
    NSLog(@"AdvertiseBannerWebView: OTRO INTENTO");
    [self loadAdvertise];
}

@end
