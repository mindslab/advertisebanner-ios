#import <UIKit/UIKit.h>
#import "AdvertiseBannerWebView.h"

extern NSString * const BannerViewActionWillBegin;
extern NSString * const BannerViewActionDidFinish;

@interface AdvertiseBannerViewController : UIViewController <AdvertiseBannerWebViewDelegate>

- (instancetype)initWithContentViewController:(UIViewController *)contentController adType:(AdvertiseBannerType)adType;

@end
