//
//  AdvertiseBannerWebView.h
//  AdvertiseBanner
//
//  Created by Carlos Thurber B. on 06/09/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import <UIKit/UIKit.h>

enum AdvertiseBannerType{
    AdvertiseBannerType_iPhone = 1,
    AdvertiseBannerType_iPad
} typedef AdvertiseBannerType;

@protocol AdvertiseBannerWebViewDelegate;

@interface AdvertiseBannerWebView : UIWebView <UIWebViewDelegate>

@property (nonatomic, weak) id<AdvertiseBannerWebViewDelegate> adBannerDelegate;
@property (nonatomic, strong) NSString *urlString;
@property (nonatomic, readonly) BOOL bannerLoaded;

- (id) initWithAdType:(AdvertiseBannerType)adType;

@end


@protocol AdvertiseBannerWebViewDelegate <NSObject>
@optional

// This method is invoked when the banner has confirmation that an ad will be presented, but before the ad
// has loaded resources necessary for presentation.
- (void)adBannerViewWillLoadAd:(AdvertiseBannerWebView *)banner;

// This method is invoked each time a banner loads a new advertisement. Once a banner has loaded an ad,
// it will display that ad until another ad is available. The delegate might implement this method if
// it wished to defer placing the banner in a view hierarchy until the banner has content to display.
- (void)adBannerViewDidLoadAd:(AdvertiseBannerWebView *)banner;

// This method will be invoked when an error has occurred attempting to get advertisement content.
// The ADError enum lists the possible error codes.
- (void)adBannerView:(AdvertiseBannerWebView *)banner didFailToReceiveAdWithError:(NSError *)error;

// This message will be sent when the user taps on the banner and some action is to be taken.
// Actions either display full screen content in a modal session or take the user to a different
// application. The delegate may return NO to block the action from taking place, but this
// should be avoided if possible because most advertisements pay significantly more when
// the action takes place and, over the longer term, repeatedly blocking actions will
// decrease the ad inventory available to the application. Applications may wish to pause video,
// audio, or other animated content while the advertisement's action executes.
- (BOOL)adBannerViewActionShouldBegin:(AdvertiseBannerWebView *)banner willLeaveApplication:(BOOL)willLeave;

// This message is sent when a modal action has completed and control is returned to the application.
// Games, media playback, and other activities that were paused in response to the beginning
// of the action should resume at this point.
- (void)adBannerViewActionDidFinish:(AdvertiseBannerWebView *)banner;

- (void)openURL:(NSURL*)url;

@end